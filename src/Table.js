import { Button, Table } from "antd";
import React from "react";

const InputTable = props => {
  //TODO: 2. indsæt Input så de renderes ved tryk på knap
  const kolonner = [
    {
      title: "Navn",
      key: "navn",
      dataIndex: "name",
      render: (text, record, index) => {
        return props.activeRow === index ? <>aktiv</> : text;
      }
    },
    {
      // TODO: 1. lav conditional rendering
      title: "Email",
      key: "email",
      dataIndex: "email",
      render: (text, record, index) => text
    },
    {
      //TODO: Bonus: lav conditional inline knapper (gem og annuler)
      title: "",
      key: "button",
      render: (text, record, index) => (
        <Button onClick={() => props.setActiveRow(index)}>Klik</Button>
      )
    }
  ];
  return <Table columns={kolonner} dataSource={props.data} />;
};

export default InputTable;
