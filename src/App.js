import { Button, Card, Input, message } from "antd";
import React, { useState } from "react";
import { useFormik } from "formik";

import InputTable from "./Table";
import "antd/dist/antd.css";
import "./App.css";

function App() {
  const [formValue, setFormValues] = useState({
    name: "",
    email: ""
  });
  const [clickedRow, setClickedRow] = useState(undefined);
  const [tableData, setTableData] = useState([
    {
      name: "Nedim",
      email: "Nedim@nc.com"
    },
    {
      name: "Ivan",
      email: "Ivan@nc.com"
    }
  ]);
  const [resultValues, setResultValues] = useState({});

  const handleSubmit = values => {
    setResultValues(values);
  };
  //TODO: 3. sæt korrekt data i initial values. Hint: index + state array
  const formik = useFormik({
    initialValues: formValue,
    onSubmit: handleSubmit,
    enableReintialize: true
  });

  return (
    <div className="App">
      <Input
        name="name"
        value={formik.values.name}
        onChange={formik.handleChange}
      />
      <Input
        name="email"
        value={formik.values.email}
        onChange={formik.handleChange}
      />

      <form onSubmit={formik.handleSubmit}>
        <InputTable
          data={tableData}
          activeRow={clickedRow}
          setActiveRow={setClickedRow}
        />
        <Button htmlType="submit">Submit</Button>
      </form>

      <Card>
        <p>{resultValues.name}</p>
        <p>{resultValues.email}</p>
      </Card>
    </div>
  );
}

export default App;
